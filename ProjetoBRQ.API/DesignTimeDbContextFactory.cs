using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ProjetoBRQ.Data.Contexto;

namespace ProjetoBRQ.API
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContexto>
    {
        public DataContexto CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<DataContexto>();

            var connectionString = configuration.GetConnectionString("DefaulConnectionString");

            builder.UseMySql(connectionString);       

            return new DataContexto(builder.Options);
        }
    }
}
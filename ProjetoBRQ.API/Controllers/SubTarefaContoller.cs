using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoBRQ.API.Dtos;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Dominio;

namespace ProjetoBRQ.API.Controllers
{

    [Route("api/[controller]")]
    [Authorize]
    public class SubTarefaController : Controller
    {
        private readonly IUnitOfWorkApp _uow;
        private readonly IMapper _mapper;
        public SubTarefaController(IUnitOfWorkApp _uow, IMapper mapper)
        {
            this._uow = _uow;
            this._mapper = mapper;

        }



        [HttpPost("cadastrar")]
        public async Task<IActionResult> Cadastrar([FromBody]SubTarefaDto subTarefaDto)
        {
           
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var subTarefaForCreate = _mapper.Map<SubTarefa>(subTarefaDto);
            subTarefaForCreate.Status = "ABERTO";
            await _uow.SubTarefaApp.AdicionarAsync(subTarefaForCreate);
            await _uow.SalvarAsync();


            var tarefaUpdate = _uow.TarefaApp.ObterPorId(subTarefaDto.Tarefa_Id);
            tarefaUpdate.Status = "ANDAMENTO";
            await _uow.SalvarAsync();

            var subTarefaForReturn = _mapper.Map<SubTarefaDto>(subTarefaForCreate);

            return Ok(subTarefaForReturn);

        }


        [HttpGet]
        [Route("obtersubtarefas/{idTarefa}")]
        public async Task<IActionResult> ObterTarefasPorUsuario(int idTarefa)
        {
            var subTarefasFromApp = await _uow.SubTarefaApp.ObterTodos().Where(x => x.Tarefa_Id == idTarefa).ToListAsync();
            var subTarefasToReturn = Mapper.Map<List<SubTarefaDto>>(subTarefasFromApp);

            return Ok(subTarefasToReturn);
        }

        [HttpGet("obterporid/{id}")]
        public async Task<IActionResult> ObterTarefaPorId(int id)
        {
            var subTarefasFromApp = await _uow.SubTarefaApp.ObterTodos().FirstOrDefaultAsync(x => x.Id == id);
            var subTarefasToReturn = Mapper.Map<SubTarefaDto>(subTarefasFromApp);


            return Ok(subTarefasToReturn);
        }

        [HttpPut]
        public async Task<IActionResult> AlterarSubTarefa([FromBody]SubTarefaDto subtarefaDto)
        {

            try
            {


                var updateForSubTarefa = _uow.SubTarefaApp.ObterPorId(subtarefaDto.Id);
                if (updateForSubTarefa.Status != "CONCLUIDO")
                {
                    
                    var tarefa = _uow.TarefaApp.ObterTodos().Include(x => x.SubTarefas).Include(x => x.Usuario).FirstOrDefault(x => x.Id == subtarefaDto.Tarefa_Id);
                    var subtarefaUpdateUpdate = updateForSubTarefa;

                    subtarefaUpdateUpdate = Mapper.Map<SubTarefaDto, SubTarefa>(subtarefaDto);
                    

                    subtarefaUpdateUpdate.Tarefa = tarefa;

                    subtarefaUpdateUpdate.Status = "CONCLUIDO";

                    var subTarefasTotal = _uow.SubTarefaApp.ObterTodos().Where(a => a.Tarefa_Id == subtarefaDto.Tarefa_Id).ToList();

                    var subTarefasConcluidas = subTarefasTotal.Where(a => a.Status == "CONCLUIDO").ToList();

                    if (subTarefasTotal.Count() == subTarefasConcluidas.Count())
                    {
                        var tarefaForUpdate = _uow.TarefaApp.ObterPorId(subtarefaDto.Tarefa_Id);
                        tarefaForUpdate.Status = "CONCLUIDO";
                        _uow.TarefaApp.Atualizar(tarefaForUpdate);
                        _uow.Salvar();

                    }
                    else if (subTarefasTotal.Count() > subTarefasConcluidas.Count())
                    {
                        var tarefaForUpdate = _uow.TarefaApp.ObterPorId(subtarefaDto.Tarefa_Id);
                        tarefaForUpdate.Status = "ANDAMENTO";
                        _uow.TarefaApp.Atualizar(tarefaForUpdate);
                        _uow.Salvar();
                    }

                    //var subtarefaUpdateUpdate = updateForSubTarefa;
                    //_uow.SubTarefaApp.Atualizar(updateForSubTarefa);
                    _uow.SubTarefaApp.AtualizarSetvalues(updateForSubTarefa, subtarefaUpdateUpdate);
                    await _uow.SalvarAsync();


                }

            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            return Ok();

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> ExcluirSubTarefa(int id)
        {
            var deleteForSubTarefa = await _uow.SubTarefaApp.ObterTodos().FirstOrDefaultAsync(x => x.Id == id);

            //if(deleteForTarefa.Status == "ABERTO")
            //{
            _uow.SubTarefaApp.Excluir(deleteForSubTarefa);
            _uow.Salvar();
            //}

            return Ok();

        }


    }
}
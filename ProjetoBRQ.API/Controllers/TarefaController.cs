using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoBRQ.API.Dtos;
using ProjetoBRQ.API.Helpers;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Data.Repositorio.PaginacaoEntity;
using ProjetoBRQ.Dominio;

namespace ProjetoBRQ.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class TarefaController : Controller
    {
        private readonly IUnitOfWorkApp _uow;
        private readonly IMapper _mapper;
        public TarefaController(IUnitOfWorkApp _uow, IMapper mapper)
        {
            this._uow = _uow;
            this._mapper = mapper;

        }

        [HttpPost("cadastrar")]
        public async Task<IActionResult> Cadastrar([FromBody]TarefaDto tarefaDto)
        {
            var tarefaForCreate = new Tarefa();


            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userFromTarefa = _uow.UsuarioApp.ObterPorId(tarefaDto.Usuario_Id);

            tarefaForCreate = _mapper.Map<Tarefa>(tarefaDto);
            tarefaForCreate.Usuario = userFromTarefa;

            await _uow.TarefaApp.AdicionarAsync(tarefaForCreate);
            await _uow.SalvarAsync();

            var tarefaForReturn = _mapper.Map<TarefaDto>(tarefaForCreate);

            return Ok(tarefaForReturn);



        }


        // [Route("obtertarefasdatatable")]
        [HttpPost("obtertarefasdatatable")]

        public async Task<IActionResult> ObterTarefasDatatable([FromBody]TarefaDto tarefaDto)
        {

            string login =  User.Identity.Name;

            var usuario = await _uow.UsuarioApp.ObterTodos().FirstOrDefaultAsync(x => x.UserName == login);

            var tarefasToReturn = new List<TarefaDto>();

            if (tarefaDto.Usuario_Id != 0)
            {

                var sortColumnIndex = Convert.ToInt32(HttpContext.Request.Query["iSortCol_0"]);
                var sortDirection = HttpContext.Request.Query["sSortDir_0"];

                var tarefasFromResult = await _uow.TarefaApp.ObterTodos().Include(a => a.SubTarefas).Where(a => a.Usuario_Id == usuario.Id).ToListAsync();
                var totalRecords = tarefasFromResult;


                if (!string.IsNullOrEmpty(tarefaDto.Search))
                {
                    tarefasFromResult = tarefasFromResult.Where(a => a.Descricao.ToUpper().Contains(tarefaDto.Search.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(tarefaDto.Descricao))
                {
                    tarefasFromResult = tarefasFromResult.Where(a => a.Descricao.ToUpper().Contains(tarefaDto.Descricao.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(tarefaDto.Processamento))
                {
                    tarefasFromResult = tarefasFromResult.Where(a => a.Status == tarefaDto.Processamento).ToList();
                }

                switch (tarefaDto.Column)
                {
                    case 0:
                        tarefasFromResult = tarefaDto.Direction == "asc"
                            ? tarefasFromResult.OrderBy(x => x.Descricao).ToList()
                            : tarefasFromResult.OrderByDescending(x => x.Descricao).ToList();
                        break;

                    case 1:
                        tarefasFromResult = tarefaDto.Direction == "asc"
                            ? tarefasFromResult.OrderBy(x => x.Status).ToList()
                            : tarefasFromResult.OrderByDescending(x => x.Status).ToList();
                        break;


                }

                tarefasToReturn = Mapper.Map<List<Tarefa>, List<TarefaDto>>(tarefasFromResult);
            }
            
            DataTablesResponse<TarefaDto> datatable = new DataTablesResponse<TarefaDto>();
            datatable.data = tarefasToReturn.ToPagedList(tarefaDto.Start / tarefaDto.Length, tarefaDto.Length).ToList();
            datatable.recordsTotal = datatable.data.Count();
            datatable.recordsFiltered = tarefasToReturn.Count();




            return Ok(datatable);
        }

        [Route("obtertarefas")]
        [HttpGet]

        public async Task<IActionResult> ObterTarefasPorUsuario([FromQuery]TarefaDto tarefaDto)
        {


            var tarefasFromResult = await _uow.TarefaApp.ObterTodos().Include(a => a.SubTarefas).Where(x => x.Usuario_Id == tarefaDto.Usuario_Id).ToListAsync();

            if (!string.IsNullOrEmpty(tarefaDto.Descricao))
            {
                tarefasFromResult = tarefasFromResult.Where(a => tarefaDto.Descricao.Contains(a.Descricao)).ToList();
            }

            if (!string.IsNullOrEmpty(tarefaDto.Processamento))
            {
                tarefasFromResult = tarefasFromResult.Where(a => a.Status == tarefaDto.Processamento).ToList();
            }

            var tarefasToReturn = Mapper.Map<List<Tarefa>, List<TarefaDto>>(tarefasFromResult);



            return Ok(tarefasToReturn);
        }

        // [Route("obterporid")]
        [HttpGet("obterporid/{id}")]
        public async Task<IActionResult> ObterTarefaPorId(int id)
        {
            var tarefaFromResult = await _uow.TarefaApp.ObterTodos().Include(a => a.SubTarefas).FirstOrDefaultAsync(x => x.Id == id);


            var tarefaToReturn = Mapper.Map<Tarefa, TarefaDto>(tarefaFromResult);
            return Ok(tarefaToReturn);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> ExcluirTarefa(int id)
        {
            var deleteForTarefa = await _uow.TarefaApp.ObterTodos().FirstOrDefaultAsync(x => x.Id == id);

            //if(deleteForTarefa.Status == "ABERTO")
            //{
            _uow.TarefaApp.Excluir(deleteForTarefa);
            _uow.Salvar();
            //}

            return Ok();

        }

        [HttpPut]
        public async Task<IActionResult> AlterarTarefa([FromBody]TarefaDto tarefaDto)
        {
            var tarefaFromApp = await _uow.TarefaApp.ObterTodos().FirstOrDefaultAsync(x => x.Id == tarefaDto.Id);
            var tarefaForUpdate = tarefaFromApp;
            if (tarefaFromApp.Status != "CONCLUIDO")
            {

                tarefaForUpdate.Status = "CONCLUIDO";

                //var usuario = updateForTarefa.Usuario;
                //updateForTarefa = Mapper.Map<TarefaDto, Tarefa>(tarefaDto);
                //updateForTarefa.Usuario = usuario;

                _uow.TarefaApp.AtualizarSetvalues(tarefaFromApp, tarefaForUpdate);
                _uow.Salvar();
            }

            return Ok();

        }

    }
}
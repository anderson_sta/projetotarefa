using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Dominio;
using AutoMapper;
using ProjetoBRQ.API.Dtos;

namespace ProjetoBRQ.API.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class AuthController : Controller
    {

        private readonly IUnitOfWorkApp _uow;
        private readonly IMapper _mapper;
        public AuthController(IUnitOfWorkApp _uow, IMapper mapper)
        {
            this._uow = _uow;
            this._mapper = mapper;

        }


        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UsuarioDto userForRegisterDTO)
        {

            try
            {


                if (!String.IsNullOrEmpty(userForRegisterDTO.UserName))
                    userForRegisterDTO.UserName = userForRegisterDTO.UserName.ToLower();

                if (await _uow.UsuarioApp.UserExists(userForRegisterDTO.UserName))
                    ModelState.AddModelError("Username", "Usuário já existe");


                if (!ModelState.IsValid)
                    return BadRequest(ModelState);



                var userToCreate = _mapper.Map<Usuario>(userForRegisterDTO);

                var createUser = await _uow.UsuarioApp.Register(userToCreate, userForRegisterDTO.Password);
                await _uow.SalvarAsync();
                var userToReturn = _mapper.Map<UsuarioDto>(createUser);

                var identityClaim = new ClaimsIdentity(new[]{
                
                 // new Claim(ClaimTypes.Email, usuario.Email),
                new Claim(ClaimTypes.Sid, createUser.Id.ToString()),
                new Claim(ClaimTypes.Name, createUser.UserName)
            }, "ApplicationCookie");

                return Ok(userToReturn);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
            //return CreatedAtRoute("GetUser", new { controller = "Users", id = createUser.Id}, userToReturn);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]UsuarioDto userForLoginDTO)
        {


            Usuario userFromRepo = await _uow.UsuarioApp.Login(userForLoginDTO.UserName, userForLoginDTO.Password);

            if (userFromRepo == null)
                return Unauthorized();

            var tokenHander = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("super secret key");
            var tokendescript = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                    new Claim(ClaimTypes.Name, userFromRepo.UserName)
                }),

                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha512Signature)

            };



            var token = tokenHander.CreateToken(tokendescript);
            var tokenString = tokenHander.WriteToken(token);
            var user = _mapper.Map<UsuarioDto>(userFromRepo);

            var identityClaim = new ClaimsIdentity(new[]{
                
                 // new Claim(ClaimTypes.Email, usuario.Email),
                new Claim(ClaimTypes.Sid, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            }, "ApplicationCookie");

            return Ok(new { tokenString, user });



        }

        public async Task<IActionResult> LogOut([FromBody]UsuarioDto userForLoginDTO)
        {



            //var ctx = HttpCRequest.GetOwinContext();
            await HttpContext.Authentication.SignOutAsync("ApplicationCookie");
            return Ok();


            // return RedirectToAction("Index", "Home");
        }




    }
}
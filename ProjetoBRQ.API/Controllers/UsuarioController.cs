using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjetoBRQ.API.Dtos;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;

namespace ProjetoBRQ.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UsuarioController : Controller
    {
        private readonly IUnitOfWorkApp _uow;
        private readonly IMapper _mapper;
        public UsuarioController(IUnitOfWorkApp uow, IMapper mapper)
        {
            _mapper = mapper;
            _uow = uow;

        }


        [HttpGet("{id}", Name="GetUser")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user = await _uow.UsuarioApp.ObterPorIdAsync(id);
            var userToReturn = _mapper.Map<UsuarioDto>(user);

           
            return Ok(userToReturn);
        }

        [HttpPut("{id}")]
         public async Task<IActionResult> UpdateUser(int id, [FromBody] UsuarioDto UserForUpdateDto   )
         {
             if(!ModelState.IsValid)
                return BadRequest(ModelState);
            
             var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

             var userFromRepo = await _uow.UsuarioApp.ObterPorIdAsync(id);

             if(userFromRepo == null)
                return NotFound($"Coul not find user with an ID of {id}");

             if(currentUserId != userFromRepo.Id)
                return Unauthorized();

             _mapper.Map(UserForUpdateDto, userFromRepo);

             await _uow.SalvarAsync();
                return NoContent();

            throw new Exception($"Updating user {id} failed on saved");
         }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProjetoBRQ.Data.Aplicacao;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Data.Contexto;
using ProjetoBRQ.Data.Repositorio;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;
using ProjetoBRQ.Dominio.Interfaces.Servico;
using ProjetoBRQ.Dominio.Servico;
using ProjetoBRQ.API.Helpers;
using AutoMapper;
using ProjetoBRQ.Dominio;
using System.IO;

namespace ProjetoBRQ.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            /* var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
              //  .Build();
            Configuration = builder.Build(); */
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
           // var connectionString = Configuration.GetConnectionString("DbCoreConnectionString");
            var key = Encoding.ASCII.GetBytes("super secret key");
            
            var connStr = Configuration.GetConnectionString("DefaulConnectionString");
            System.Console.WriteLine(connStr);

            /* services.AddDbContext<DataContexto>(x => x.UseSqlServer(
                Configuration.GetConnectionString("DefaulConnectionString")
                ,
                y => y.MigrationsAssembly("ProjetoBRQ.Data.Contexto") 

            )); */
            //services.AddDbContext<DataContexto>(x => x.UseSqlServer("DefaulConnectionString"));

            services.AddTransient(typeof(DataContexto));
            services.AddTransient(typeof(DbContextOptions<DataContexto>));

            services.AddCors();
            services.AddAutoMapper();
            services.AddMvc().AddJsonOptions(opt =>
            {
                opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });


            services.AddScoped<IRepositorioBase<Tarefa>, RepositorioBase<Tarefa>>();
            services.AddScoped<IRepositorioBase<SubTarefa>, RepositorioBase<SubTarefa>>();
            services.AddScoped<IRepositorioBase<Usuario>, RepositorioBase<Usuario>>();

            services.AddScoped<IServicoBase<Tarefa>, ServicoBase<Tarefa>>();
            services.AddScoped<IServicoBase<SubTarefa>, ServicoBase<SubTarefa>>();
            services.AddScoped<IServicoBase<Usuario>, ServicoBase<Usuario>>();

            services.AddScoped<IAppBase<Tarefa>, AppBase<Tarefa>>();
            services.AddScoped<IAppBase<SubTarefa>, AppBase<SubTarefa>>();
            services.AddScoped<IAppBase<Usuario>, AppBase<Usuario>>();


            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUsuarioRepositorio, UsuarioRepositorio>();
            services.AddScoped<ITarefaRepositorio, TarefaRepositorio>();
            services.AddScoped<ISubTarefaRepositorio, SubTarefaRepositorio>();

            services.AddScoped<IUnitOfWorkServico, UnitOfWorkServico>();
            services.AddScoped<IUsuarioServico, UsuarioServico>();
            services.AddScoped<ITarefaServico, TarefaServico>();
            services.AddScoped<ISubTarefaServico, SubTarefaServico>();

            services.AddScoped<IUnitOfWorkApp, UnitOfWorkApp>();
            services.AddScoped<IUsuarioApp, UsuarioApp>();
            services.AddScoped<ITarefaApp, TarefaApp>();
            services.AddScoped<ISubTarefaApp, SubTarefaApp>();



            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });


            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(builder =>
                {
                    builder.Run(async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        var error = context.Features.Get<IExceptionHandlerFeature>();

                        if (error != null)
                        {
                            context.Response.AddApplicationError(error.Error.Message);
                            await context.Response.WriteAsync(error.Error.Message);
                        }
                    });
                });
            }


            app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}

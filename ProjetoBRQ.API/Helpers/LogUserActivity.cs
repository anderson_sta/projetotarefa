using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using ProjetoBRQ.Data.Aplicacao.Interface;

namespace ProjetoBRQ.API.Helpers
{
    public class LogUserActivity : IAsyncActionFilter
    {

        private readonly IUnitOfWorkApp _uow;
        public LogUserActivity(IUnitOfWorkApp uow)
        {
            _uow = uow;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var resultContext = await next();

            var userId = int.Parse(resultContext.HttpContext.User
            .FindFirst(ClaimTypes.NameIdentifier).Value);

            //var repo = resultContext.HttpContext.RequestServices.GetService<IUnitOfWorkApp>();
            var user = await _uow.UsuarioApp.ObterPorIdAsync(userId);

            //user.LastActive = DateTime.Now;
            await _uow.SalvarAsync();

        }
    }
}
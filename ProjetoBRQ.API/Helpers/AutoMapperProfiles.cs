using AutoMapper;
using ProjetoBRQ.API.Dtos;
using ProjetoBRQ.Dominio;

namespace ProjetoBRQ.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Usuario, UsuarioDto>()
            .ForMember(dest => dest.Tarefas, opt => {
                opt.MapFrom(src => src.Tarefas);
            })
            
            ;
            
            CreateMap<Tarefa, TarefaDto>()
            .ForMember(dest => dest.Usuario, opt => {
                opt.MapFrom(src => src.Usuario);
            })
            .ForMember(dest => dest.Processamento, opt => {
                opt.MapFrom(src => src.Status);
            })
            .ForMember(dest => dest.SubTarefas, opt => {
                opt.MapFrom(src => src.SubTarefas);
            });
            

            CreateMap<SubTarefa, SubTarefaDto>()
            .ForMember(dest => dest.Tarefa, opt => {
                opt.MapFrom(src => src.Tarefa);
            })
            .ForMember(dest => dest.Processamento, opt => {
                opt.MapFrom(src => src.Status);
            });
            

            CreateMap<UsuarioDto, Usuario>();
            
            CreateMap<TarefaDto, Tarefa>()
            .ForMember(dest => dest.Status, opt => {
                opt.MapFrom(src => src.Processamento);
            })
            .ForMember(dest => dest.Usuario, opt => {
                opt.MapFrom(src => src.Usuario);
            });

            CreateMap<SubTarefaDto, SubTarefa>()
            .ForMember(dest => dest.Status, opt => {
                opt.MapFrom(src => src.Processamento);
            });

            

        }
    }
}
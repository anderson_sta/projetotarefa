namespace ProjetoBRQ.API.Dtos
{
    public class SubTarefaDto
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public TarefaDto Tarefa { get; set; }

        public string Processamento { get; set; }
        public int Tarefa_Id { get; set;  }
    }
}
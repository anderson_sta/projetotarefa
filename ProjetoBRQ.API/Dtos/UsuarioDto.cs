using System;
using System.Collections.Generic;

namespace ProjetoBRQ.API.Dtos
{
    public class UsuarioDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public byte[] Passwordhash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTime DataCriacao { get; set; }
        public ICollection<TarefaDto> Tarefas { get; set; }
    }
}
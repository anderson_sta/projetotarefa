using System.Collections.Generic;
using System.Linq;

namespace ProjetoBRQ.API.Dtos
{
    public class TarefaDto
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string Processamento { get; set; }
        public int Usuario_Id { get; set; }
        public string Percentual { get 
        { 
            

                if(Processamento == "ABERTO")
                    return "0";
                else if(Processamento == "CONCLUIDO")
                    return "100";
                else if(this.SubTarefas != null && this.SubTarefas.Count() > 0)
                    return ((decimal.Parse(this.SubTarefas.Where(x => x.Processamento == "CONCLUIDO").ToList().Count().ToString()) / decimal.Parse(this.SubTarefas.ToList().Count().ToString())) * 100).ToString("0.##");
                else
                    return "0";
          
        }}

        public UsuarioDto Usuario {get; set;}
        public ICollection<SubTarefaDto> SubTarefas { get; set; }

        public int Column { get; set; }

        public int Length { get; set; }

        public int Start { get; set; }

        public string Direction { get; set; }

        public string Search { get; set; }
        
    }
}
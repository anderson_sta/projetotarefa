﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ProjetoBRQ.Data.Contexto.Migrations
{
    public partial class nomesTabelas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemPedido_Pedido_IdPedido",
                table: "ItemPedido");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemPedido_Produto_IdProduto",
                table: "ItemPedido");

            migrationBuilder.DropForeignKey(
                name: "FK_Pedido_Usuario_IdCliente",
                table: "Pedido");

            migrationBuilder.DropForeignKey(
                name: "FK_Pedido_StatusPedido_IdStatusPedido",
                table: "Pedido");

            migrationBuilder.DropForeignKey(
                name: "FK_SubTarefa_Tarefa_Tarefa_Id",
                table: "SubTarefa");

            migrationBuilder.DropForeignKey(
                name: "FK_Tarefa_Usuario_Usuario_Id",
                table: "Tarefa");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Usuario",
                table: "Usuario");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tarefa",
                table: "Tarefa");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubTarefa",
                table: "SubTarefa");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StatusPedido",
                table: "StatusPedido");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Produto",
                table: "Produto");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Pedido",
                table: "Pedido");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ItemPedido",
                table: "ItemPedido");

            migrationBuilder.RenameTable(
                name: "Usuario",
                newName: "usuario");

            migrationBuilder.RenameTable(
                name: "Tarefa",
                newName: "tarefa");

            migrationBuilder.RenameTable(
                name: "SubTarefa",
                newName: "subtarefa");

            migrationBuilder.RenameTable(
                name: "StatusPedido",
                newName: "statuspedido");

            migrationBuilder.RenameTable(
                name: "Produto",
                newName: "produto");

            migrationBuilder.RenameTable(
                name: "Pedido",
                newName: "pedido");

            migrationBuilder.RenameTable(
                name: "ItemPedido",
                newName: "itempedido");

            migrationBuilder.RenameIndex(
                name: "IX_Tarefa_Usuario_Id",
                table: "tarefa",
                newName: "IX_tarefa_Usuario_Id");

            migrationBuilder.RenameIndex(
                name: "IX_SubTarefa_Tarefa_Id",
                table: "subtarefa",
                newName: "IX_subtarefa_Tarefa_Id");

            migrationBuilder.RenameIndex(
                name: "IX_Pedido_IdStatusPedido",
                table: "pedido",
                newName: "IX_pedido_IdStatusPedido");

            migrationBuilder.RenameIndex(
                name: "IX_Pedido_IdCliente",
                table: "pedido",
                newName: "IX_pedido_IdCliente");

            migrationBuilder.RenameIndex(
                name: "IX_Pedido_DataCriacao",
                table: "pedido",
                newName: "IX_pedido_DataCriacao");

            migrationBuilder.RenameIndex(
                name: "IX_ItemPedido_IdProduto",
                table: "itempedido",
                newName: "IX_itempedido_IdProduto");

            migrationBuilder.RenameIndex(
                name: "IX_ItemPedido_IdPedido",
                table: "itempedido",
                newName: "IX_itempedido_IdPedido");

            migrationBuilder.AddPrimaryKey(
                name: "PK_usuario",
                table: "usuario",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_tarefa",
                table: "tarefa",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_subtarefa",
                table: "subtarefa",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_statuspedido",
                table: "statuspedido",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_produto",
                table: "produto",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_pedido",
                table: "pedido",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_itempedido",
                table: "itempedido",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_itempedido_pedido_IdPedido",
                table: "itempedido",
                column: "IdPedido",
                principalTable: "pedido",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_itempedido_produto_IdProduto",
                table: "itempedido",
                column: "IdProduto",
                principalTable: "produto",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_pedido_usuario_IdCliente",
                table: "pedido",
                column: "IdCliente",
                principalTable: "usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_pedido_statuspedido_IdStatusPedido",
                table: "pedido",
                column: "IdStatusPedido",
                principalTable: "statuspedido",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_subtarefa_tarefa_Tarefa_Id",
                table: "subtarefa",
                column: "Tarefa_Id",
                principalTable: "tarefa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_tarefa_usuario_Usuario_Id",
                table: "tarefa",
                column: "Usuario_Id",
                principalTable: "usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_itempedido_pedido_IdPedido",
                table: "itempedido");

            migrationBuilder.DropForeignKey(
                name: "FK_itempedido_produto_IdProduto",
                table: "itempedido");

            migrationBuilder.DropForeignKey(
                name: "FK_pedido_usuario_IdCliente",
                table: "pedido");

            migrationBuilder.DropForeignKey(
                name: "FK_pedido_statuspedido_IdStatusPedido",
                table: "pedido");

            migrationBuilder.DropForeignKey(
                name: "FK_subtarefa_tarefa_Tarefa_Id",
                table: "subtarefa");

            migrationBuilder.DropForeignKey(
                name: "FK_tarefa_usuario_Usuario_Id",
                table: "tarefa");

            migrationBuilder.DropPrimaryKey(
                name: "PK_usuario",
                table: "usuario");

            migrationBuilder.DropPrimaryKey(
                name: "PK_tarefa",
                table: "tarefa");

            migrationBuilder.DropPrimaryKey(
                name: "PK_subtarefa",
                table: "subtarefa");

            migrationBuilder.DropPrimaryKey(
                name: "PK_statuspedido",
                table: "statuspedido");

            migrationBuilder.DropPrimaryKey(
                name: "PK_produto",
                table: "produto");

            migrationBuilder.DropPrimaryKey(
                name: "PK_pedido",
                table: "pedido");

            migrationBuilder.DropPrimaryKey(
                name: "PK_itempedido",
                table: "itempedido");

            migrationBuilder.RenameTable(
                name: "usuario",
                newName: "Usuario");

            migrationBuilder.RenameTable(
                name: "tarefa",
                newName: "Tarefa");

            migrationBuilder.RenameTable(
                name: "subtarefa",
                newName: "SubTarefa");

            migrationBuilder.RenameTable(
                name: "statuspedido",
                newName: "StatusPedido");

            migrationBuilder.RenameTable(
                name: "produto",
                newName: "Produto");

            migrationBuilder.RenameTable(
                name: "pedido",
                newName: "Pedido");

            migrationBuilder.RenameTable(
                name: "itempedido",
                newName: "ItemPedido");

            migrationBuilder.RenameIndex(
                name: "IX_tarefa_Usuario_Id",
                table: "Tarefa",
                newName: "IX_Tarefa_Usuario_Id");

            migrationBuilder.RenameIndex(
                name: "IX_subtarefa_Tarefa_Id",
                table: "SubTarefa",
                newName: "IX_SubTarefa_Tarefa_Id");

            migrationBuilder.RenameIndex(
                name: "IX_pedido_IdStatusPedido",
                table: "Pedido",
                newName: "IX_Pedido_IdStatusPedido");

            migrationBuilder.RenameIndex(
                name: "IX_pedido_IdCliente",
                table: "Pedido",
                newName: "IX_Pedido_IdCliente");

            migrationBuilder.RenameIndex(
                name: "IX_pedido_DataCriacao",
                table: "Pedido",
                newName: "IX_Pedido_DataCriacao");

            migrationBuilder.RenameIndex(
                name: "IX_itempedido_IdProduto",
                table: "ItemPedido",
                newName: "IX_ItemPedido_IdProduto");

            migrationBuilder.RenameIndex(
                name: "IX_itempedido_IdPedido",
                table: "ItemPedido",
                newName: "IX_ItemPedido_IdPedido");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Usuario",
                table: "Usuario",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tarefa",
                table: "Tarefa",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubTarefa",
                table: "SubTarefa",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StatusPedido",
                table: "StatusPedido",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Produto",
                table: "Produto",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Pedido",
                table: "Pedido",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ItemPedido",
                table: "ItemPedido",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPedido_Pedido_IdPedido",
                table: "ItemPedido",
                column: "IdPedido",
                principalTable: "Pedido",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPedido_Produto_IdProduto",
                table: "ItemPedido",
                column: "IdProduto",
                principalTable: "Produto",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pedido_Usuario_IdCliente",
                table: "Pedido",
                column: "IdCliente",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pedido_StatusPedido_IdStatusPedido",
                table: "Pedido",
                column: "IdStatusPedido",
                principalTable: "StatusPedido",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubTarefa_Tarefa_Tarefa_Id",
                table: "SubTarefa",
                column: "Tarefa_Id",
                principalTable: "Tarefa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tarefa_Usuario_Usuario_Id",
                table: "Tarefa",
                column: "Usuario_Id",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

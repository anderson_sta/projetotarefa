using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using ProjetoBRQ.Dominio;
using ProjetoBRQ.Dominio.Interfaces;
using Microsoft.Extensions.Configuration;


namespace ProjetoBRQ.Data.Contexto
{
    public class DataContexto : DbContext
    {
        // public IConfiguration Configuration { get; }
        //public DataContexto() { }
        public DataContexto(DbContextOptions<DataContexto> options) : base(options)
        {
            //Configuration = configurartion;
        }

        public DbSet<Usuario> Usuario { get; set; }

        public DbSet<Tarefa> Tarefa { get; set; }

        public DbSet<SubTarefa> SubTarefa { get; set; }

        public DbSet<Pedido> Pedido { get; set; }
        public DbSet<ItemPedido> ItemPedido { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<StatusPedido> StatusPedido { get; set; }

         protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
         {

             //var connectionString = optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaulConnectionString"));
             //System.Console.WriteLine(connectionString);

             optionsBuilder.UseMySql("Server=mysql.andersontavares.com.br;User Id=andersontavare;Password=pic8201and;Database=andersontavare");
             //optionsBuilder.UseMySql("Server=localhost;User Id=root;Password=123456;Database=aplicacaoangular");
             base.OnConfiguring(optionsBuilder);


         }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<Usuario>(
                
                u =>
                {
                    u.ToTable("usuario");
                    u.HasKey(x => x.Id);
                    u.HasMany(x => x.Tarefas)
                    .WithOne(t => t.Usuario)
                    .HasForeignKey(f => f.Usuario_Id);
                }
            );

            modelBuilder.Entity<Tarefa>(
                u =>
                {
                    u.ToTable("tarefa");
                    u.HasKey(x => x.Id);
                    u.HasMany(x => x.SubTarefas)
                    .WithOne(t => t.Tarefa)
                    .HasForeignKey(f => f.Tarefa_Id);
                }
            );

            modelBuilder.Entity<SubTarefa>(
                u =>
                {
                    u.ToTable("subtarefa");
                    u.HasKey(x => x.Id);

                }
            );


            modelBuilder.Entity<Pedido>(
                p =>
                {
                    p.ToTable("pedido");
                    p.HasKey(x => x.Id);

                    p.HasMany(x => x.ItensPedido)
                        .WithOne(i => i.Pedido)
                        .HasForeignKey(f => f.IdPedido);

                    p.HasOne(x => x.Usuario)
                        .WithMany(u => u.Pedidos)
                        .HasForeignKey(f => f.IdCliente);

                    p.HasOne(x => x.StatusPedido)
                        .WithMany(s => s.Pedido)
                        .HasForeignKey(f => f.IdStatusPedido);

                    p.HasIndex(x => x.DataCriacao);
                }

            );



            modelBuilder.Entity<ItemPedido>(
                i =>
                {
                    i.ToTable("itempedido");
                    i.HasKey(x => x.Id);

                    i.HasOne(x => x.Produto)
                        .WithMany(p => p.ItensPedido)
                        .HasForeignKey(f => f.IdProduto);

                    i.Ignore(x => x.Deletado);
                    i.Ignore(x => x.FlagIncluso);
                }

            );

            modelBuilder.Entity<Produto>(

                p =>
                {
                    p.ToTable("produto");
                    p.HasKey(x => x.Id);
                }
            );


            modelBuilder.Entity<StatusPedido>(


                s =>
                {
                    s.ToTable("statuspedido");
                    s.HasKey(x => x.Id);
                }
            );




            base.OnModelCreating(modelBuilder);
        }


        public void Rollback()
        {
            ChangeTracker.Entries().Where(x => x.State != EntityState.Added).ToList().ForEach(x => x.Reload());
        }

        
    }
}
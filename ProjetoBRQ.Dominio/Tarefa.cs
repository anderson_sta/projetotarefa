using System.Collections.Generic;

namespace ProjetoBRQ.Dominio
{
    public class Tarefa
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public virtual Usuario Usuario {get; set;}

        public int Usuario_Id { get; set; }

        public ICollection<SubTarefa> SubTarefas { get; set; }
        public string Status { get; set; }



    }
}
namespace ProjetoBRQ.Dominio
{
    public class ItemPedido
    {
        public int Id { get; set; }

        public int IdPedido { get; set; }

        public virtual Pedido Pedido { get; set; }

        public int IdProduto { get; set; }

        public virtual Produto Produto { get; set; }

        public decimal ValorItem { get; set; }

        public decimal ValorDesconto { get; set; }

        public decimal PrecoUnitario { get; set; }

        public int Quantidade { get; set; }

        public bool Deletado { get; set; }

        public bool FlagIncluso { get; set; }




    }
}
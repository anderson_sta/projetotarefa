using Microsoft.EntityFrameworkCore;

namespace ProjetoBRQ.Dominio.Interfaces
{
    public interface IObjectContextAdapter
    {
          //
        // Resumo:
        //     Gets the object context.
        DbContext ObjectContext { get; }
    }
}
using System.Threading.Tasks;

namespace ProjetoBRQ.Dominio.Interfaces.Repositorio
{
    public interface IUnitOfWork
    {
          #region Propriedades

        IUsuarioRepositorio UsuarioRepositorio { get; }
        ITarefaRepositorio TarefaRepositorio { get; }
        ISubTarefaRepositorio SubTarefaRepositorio { get; }

        #endregion

        void Salvar();
        Task SalvarAsync();
        void Dispose();
    }
}
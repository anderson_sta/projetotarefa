using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoBRQ.Dominio.Interfaces.Repositorio
{
    public interface IRepositorioBase<T> where T : class 
    {
        IQueryable<T> ObterTodos();

       
        T ObterPorId(int id);
        Task<T> ObterPorIdAsync(int id);
        void Adicionar(T entity);
        void Atualizar(T entity);
        void AtualizarSetvalues(T entity, T setValues);

        void Excluir(T entity);
        Task AdicionarAsync(T entity);
        void Dispose();
    }
}
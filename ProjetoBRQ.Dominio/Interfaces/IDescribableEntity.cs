namespace ProjetoBRQ.Dominio.Interfaces
{
    public interface IDescribableEntity
    {
         string Describe();
    }
}
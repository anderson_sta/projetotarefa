using System.Threading.Tasks;

namespace ProjetoBRQ.Dominio.Interfaces.Servico
{
    public interface IUnitOfWorkServico
    {
         IUsuarioServico UsuarioServico  { get; }
         ITarefaServico TarefaServico { get; }
         ISubTarefaServico SubTarefaServico { get; }

         void Salvar();
         Task SalvarAsync();
         void RefreshContext();
    }
}
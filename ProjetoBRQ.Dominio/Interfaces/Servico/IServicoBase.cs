using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoBRQ.Dominio.Interfaces.Servico
{
    public interface IServicoBase<T> where T: class
    {

        
        IQueryable<T> ObterTodos();
        T ObterPorId(int id);
        void Adicionar(T obj);
        void Excluir(T obj);
        void Atualizar(T obj);

        void AtualizarSetvalues(T entity, T setValues);
       
        Task AdicionarAsync(T obj);

         Task<T> ObterPorIdAsync(int id);
        void Dispose();
    }
}
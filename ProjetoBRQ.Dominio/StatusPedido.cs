using System.Collections.Generic;

namespace ProjetoBRQ.Dominio
{
    public class StatusPedido
    {
        public int Id { get; set; }

        public int Descricao { get; set; }

        public virtual ICollection<Pedido> Pedido { get; set; }
    }
}
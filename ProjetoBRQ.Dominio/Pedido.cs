using System;
using System.Collections.Generic;

namespace ProjetoBRQ.Dominio
{
    public class Pedido
    {
        public int Id { get; set; } 

        public int IdCliente { get; set; }    

        public virtual Usuario Usuario { get; set; }

        public DateTime DataCriacao { get; set; }  

        public decimal ValorTotal { get; set; }      

        public decimal ValorTotalDEscontos { get; set; }

        public int IdStatusPedido { get; set; }

        public virtual StatusPedido StatusPedido { get; set; }

        public virtual ICollection<ItemPedido> ItensPedido { get; set; }
    }
}
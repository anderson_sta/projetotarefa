using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Dominio.Servico
{
    public class ServicoBase<T> : IDisposable, IServicoBase<T> where T : class
    {
        private readonly IRepositorioBase<T> _db;

       

        public ServicoBase(IRepositorioBase<T> db)
        {
            _db = db;
           
        }
        public void Adicionar(T obj)
        {
            _db.Adicionar(obj);
        }

        public async Task AdicionarAsync(T obj)
        {
            await  _db.AdicionarAsync(obj);
        }

        public void Atualizar(T obj)
        {
            _db.Atualizar(obj);
        }

        public void Dispose()
        {
           _db.Dispose();
        }

        public void Excluir(T obj)
        {
           _db.Excluir(obj);
        }

        public T ObterPorId(int id)
        {
            return _db.ObterPorId(id);
        }

        public async Task<T> ObterPorIdAsync(int id)
        {
            return await _db.ObterPorIdAsync(id);
        }
        public IQueryable<T> ObterTodos()
        {
            return _db.ObterTodos();
        }

        public  void AtualizarSetvalues(T entity, T setValues)
        {
            _db.AtualizarSetvalues(entity, setValues);
        }

       

       
    }
}
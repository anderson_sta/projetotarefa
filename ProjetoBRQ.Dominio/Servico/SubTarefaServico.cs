using ProjetoBRQ.Dominio.Interfaces.Repositorio;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Dominio.Servico
{
    public class SubTarefaServico : ServicoBase<SubTarefa>, ISubTarefaServico
    {
        private readonly IRepositorioBase<SubTarefa> _db;
        public SubTarefaServico(IRepositorioBase<SubTarefa> db) : base(db)
        {
            _db = db;
        }
    }
}
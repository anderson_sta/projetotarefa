using System.Threading.Tasks;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Dominio.Servico
{
    public class UnitOfWorkServico : IUnitOfWorkServico
    {

        private readonly IUnitOfWork _db;

        public UnitOfWorkServico(IUnitOfWork db)
        {
            _db = db;
        }

        private IUsuarioServico _usuarioServico;
        private ITarefaServico _tarefaServico;

         private ISubTarefaServico _subTarefaServico;

        public IUsuarioServico UsuarioServico 
        {
            get {return _usuarioServico ?? new UsuarioServico(_db.UsuarioRepositorio); }
        }
        
        public ITarefaServico TarefaServico 
        {
            get { return _tarefaServico ?? new TarefaServico(_db.TarefaRepositorio); }
  
        }
        public ISubTarefaServico SubTarefaServico 
        {
            get { return _subTarefaServico ?? new SubTarefaServico(_db.SubTarefaRepositorio); }
  
        }
        public void RefreshContext()
        {
            throw new System.NotImplementedException();
        }

        public void Salvar()
        {
            _db.Salvar();
        }

        public async Task SalvarAsync()
        {
            await _db.SalvarAsync();
        }
    }
}
using ProjetoBRQ.Dominio.Interfaces.Repositorio;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Dominio.Servico
{

    public class TarefaServico : ServicoBase<Tarefa>, ITarefaServico
    {

        private readonly IRepositorioBase<Tarefa> _db;
        public TarefaServico(IRepositorioBase<Tarefa> db) : base(db)
        {
            _db = db;                    
        }
    }
}
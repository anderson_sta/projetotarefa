using ProjetoBRQ.Dominio.Interfaces.Repositorio;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Dominio.Servico
{
    public class UsuarioServico : ServicoBase<Usuario>, IUsuarioServico
    {
        private readonly IRepositorioBase<Usuario> _db;
        public UsuarioServico(IRepositorioBase<Usuario> db) : base(db)
        {
            _db = db;
        }
    }
}
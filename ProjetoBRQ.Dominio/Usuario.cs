using System;
using System.Collections.Generic;

namespace ProjetoBRQ.Dominio
{
    public class Usuario
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public byte[] Passwordhash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTime DataCriacao { get; set; }
        public ICollection<Tarefa> Tarefas { get; set; }

        public virtual ICollection<Pedido> Pedidos { get; set; }

        // public string teste { get; set; }
    }
}
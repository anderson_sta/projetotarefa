namespace ProjetoBRQ.Dominio
{
    public class SubTarefa
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public virtual Tarefa Tarefa { get; set; }

         public string Status { get; set; }
         public int Tarefa_Id { get; set; }
    }
}
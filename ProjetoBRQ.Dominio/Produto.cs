using System.Collections.Generic;

namespace ProjetoBRQ.Dominio
{
    public class Produto
    {
        public int Id { get; set; }

        public string Descricao { get; set; }

        public decimal PrecoUnitario { get; set; }

        public virtual ICollection<ItemPedido> ItensPedido { get; set; }
    }
}
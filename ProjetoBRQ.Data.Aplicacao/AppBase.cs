using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Data.Aplicacao
{
    public class AppBase<T> : IAppBase<T> where T : class
    {
        private readonly IServicoBase<T> _db;
        public AppBase(IServicoBase<T> db)
        {
            _db = db;
        }

        public void Adicionar(T obj)
        {
           _db.Adicionar(obj);
        }

        public async Task AdicionarAsync(T obj)
        {
           await _db.AdicionarAsync(obj);
        }

        public async Task<T> ObterPorIdAsync(int id)
        {
            return await _db.ObterPorIdAsync(id);
        }

        public void Atualizar(T obj)
        {
            _db.Atualizar(obj);
        }

        public  void AtualizarSetvalues(T entity, T setValues)
        {
            _db.AtualizarSetvalues(entity, setValues);
        }
        public void Dispose()
        {
            _db.Dispose();
        }

        public void Excluir(T obj)
        {
            _db.Excluir(obj);
        }

        public T ObterPorId(int id)
        {
            return _db.ObterPorId(id);
        }

        public IQueryable<T> ObterTodos()
        {
            return _db.ObterTodos();
        }

       
    }
}
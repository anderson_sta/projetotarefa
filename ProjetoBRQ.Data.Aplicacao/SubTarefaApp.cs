using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Dominio;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Data.Aplicacao
{
    public class SubTarefaApp : AppBase<SubTarefa>, ISubTarefaApp
    {

        private readonly IServicoBase<SubTarefa> _db;
        public SubTarefaApp(IServicoBase<SubTarefa> db) : base(db)
        {
            _db = db;
        }
    }
}
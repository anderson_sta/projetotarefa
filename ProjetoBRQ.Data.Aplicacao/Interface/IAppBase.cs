using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoBRQ.Data.Aplicacao.Interface
{
    public interface IAppBase<T> where T: class
    {
        IQueryable<T> ObterTodos();
        T ObterPorId(int id);
        void Adicionar(T obj);
        void Excluir(T obj);
        void Atualizar(T obj);

        Task AdicionarAsync(T obj);

        Task<T> ObterPorIdAsync(int id);

        void AtualizarSetvalues(T entity, T setValues);
       
        void Dispose();
    }
}
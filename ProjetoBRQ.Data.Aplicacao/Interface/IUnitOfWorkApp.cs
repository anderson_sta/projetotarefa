using System.Threading.Tasks;

namespace ProjetoBRQ.Data.Aplicacao.Interface
{
    public interface IUnitOfWorkApp 
    {
         IUsuarioApp UsuarioApp  { get; }
         ITarefaApp TarefaApp { get; }
         ISubTarefaApp SubTarefaApp { get; }

         void Salvar();
         Task SalvarAsync();
    }
}
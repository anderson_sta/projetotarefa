using System.Threading.Tasks;
using ProjetoBRQ.Dominio;

namespace ProjetoBRQ.Data.Aplicacao.Interface
{
    public interface IUsuarioApp : IAppBase<Usuario>
    {
         Task<Usuario> Register(Usuario user, string password);
         Task<Usuario> Login(string username, string password);

         Task<bool> UserExists(string username);
    }
}
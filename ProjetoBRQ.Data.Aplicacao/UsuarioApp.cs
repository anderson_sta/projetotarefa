using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Dominio;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Data.Aplicacao
{
    public class UsuarioApp : AppBase<Usuario>, IUsuarioApp
    {

        private readonly IServicoBase<Usuario> _db;
        public UsuarioApp(IServicoBase<Usuario> db) : base(db)
        {
            _db = db;
        }


        public async Task<Usuario> Register(Usuario user, string password)
        {
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.Passwordhash = passwordHash;
            user.PasswordSalt = passwordSalt;

            await _db.AdicionarAsync(user);
            //await _db.SaveChangesAsync();

            return user;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

            }
        }



        public async Task<Usuario> Login(string username, string password)
        {
            var user = await _db.ObterTodos().FirstOrDefaultAsync(x => x.UserName == username);

            if (user == null)
                return null;

            if (!VerifyExistsPasswordhas(password, user.Passwordhash, user.PasswordSalt))
                return null;

            return user;
        }

        private bool VerifyExistsPasswordhas(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {

                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }

            }

            return true;
        }

        public async Task<bool> UserExists(string username)
        {
            if (await _db.ObterTodos().AnyAsync(x => x.UserName.ToLower() == username.ToLower()))
                return true;

            return false;
        }
    }
}
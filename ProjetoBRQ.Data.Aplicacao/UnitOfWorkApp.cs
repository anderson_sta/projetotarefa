using System.Threading.Tasks;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Data.Aplicacao
{
    public class UnitOfWorkApp : IUnitOfWorkApp
    {


        private readonly IUnitOfWorkServico _db;
        public UnitOfWorkApp(IUnitOfWorkServico db)
        {
            _db = db;
        }

        private IUsuarioApp _usuarioApp;
        private ITarefaApp _tarefaApp;

        private ISubTarefaApp _subTarefaApp;

        public IUsuarioApp UsuarioApp 
        {
            get { return _usuarioApp ?? new UsuarioApp(_db.UsuarioServico); }
        }
        
        public ITarefaApp TarefaApp
        {
             get { return _tarefaApp ?? new TarefaApp(_db.TarefaServico); }
        }
        public ISubTarefaApp SubTarefaApp 
        {
             get { return _subTarefaApp ?? new SubTarefaApp(_db.SubTarefaServico); }
        }
        public void Salvar()
        {
            _db.Salvar();
        }

        public async Task SalvarAsync()
        {
            await _db.SalvarAsync();
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjetoBRQ.Data.Aplicacao.Interface;
using ProjetoBRQ.Dominio;
using ProjetoBRQ.Dominio.Interfaces.Servico;

namespace ProjetoBRQ.Data.Aplicacao
{
    public class TarefaApp : AppBase<Tarefa>, ITarefaApp
    {
        private readonly  IServicoBase<Tarefa> _db;
        public TarefaApp(IServicoBase<Tarefa> db) : base(db)
        {
            _db = db;
        }

        
    }
}
using ProjetoBRQ.Data.Contexto;
using ProjetoBRQ.Dominio;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;

namespace ProjetoBRQ.Data.Repositorio
{
    public class SubTarefaRepositorio 
        : RepositorioBase<SubTarefa>, ISubTarefaRepositorio
    {
        public SubTarefaRepositorio(DataContexto db) : base(db)
        {
                
        }
    }
}
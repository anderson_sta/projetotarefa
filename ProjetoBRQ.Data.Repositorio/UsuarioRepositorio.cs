using ProjetoBRQ.Data.Contexto;
using ProjetoBRQ.Dominio;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;

namespace ProjetoBRQ.Data.Repositorio
{
    public class UsuarioRepositorio 
        : RepositorioBase<Usuario>, IUsuarioRepositorio
    {
        public UsuarioRepositorio(DataContexto db) : base(db)
        {
        }
    }
}
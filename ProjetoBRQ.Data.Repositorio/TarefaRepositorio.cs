using ProjetoBRQ.Data.Contexto;
using ProjetoBRQ.Dominio;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;

namespace ProjetoBRQ.Data.Repositorio
{
    public class TarefaRepositorio 
        : RepositorioBase<Tarefa>, ITarefaRepositorio
    {
        public TarefaRepositorio(DataContexto db) : base(db)
        {
        }
    }
}
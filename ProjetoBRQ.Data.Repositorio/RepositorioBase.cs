using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetoBRQ.Data.Contexto;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;

namespace ProjetoBRQ.Data.Repositorio
{
    public class RepositorioBase<T> : IDisposable,
    IRepositorioBase<T> where T : class
    {
        private DataContexto _db;
        private DbSet<T> _set;

        public RepositorioBase(DataContexto db)
        {
            _db = db;
        }

        public void Adicionar(T entity)
        {
            _db.Set<T>().Add(entity);
        }

        public async Task AdicionarAsync(T entity)
        {
            await _db.Set<T>().AddAsync(entity);
        }

        public void Atualizar(T entity)
        {
           // _db.Update(entity);
             _db.Entry(entity).State = EntityState.Modified;
             _db.Set<T>().Attach(entity);
             _db.SaveChanges();
            
        }

        public void AtualizarSetvalues(T entity, T setValues)
        {
             _db.Entry(entity).CurrentValues.SetValues(setValues);
             _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public void Excluir(T entity)
        {
             _db.Set<T>().Remove(entity);
        }

        public T ObterPorId(int id)
        {
            return _db.Set<T>().Find(id);
        }

        public async Task<T> ObterPorIdAsync(int id)
        {
            return await _db.Set<T>().FindAsync(id);
        }

        public  System.Linq.IQueryable<T> ObterTodos()
        {
            return  _db.Set<T>();
        }

       
        
    }
}
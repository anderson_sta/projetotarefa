using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjetoBRQ.Data.Contexto;
using ProjetoBRQ.Dominio.Interfaces.Repositorio;

namespace ProjetoBRQ.Data.Repositorio
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {


        #region Construtor
        private readonly DataContexto _db;

        public UnitOfWork(DataContexto db)
        {
            _db = db;
        }

        /* public UnitOfWork()
        {
           // _db = new DataContexto();
        } */



        #endregion

        private IUsuarioRepositorio _usuarioRepositorio;
        private ITarefaRepositorio _tarefaRepositorio;
        private ISubTarefaRepositorio _subTarefaRepositorio;


        public IUsuarioRepositorio UsuarioRepositorio 
        {
            get { return _usuarioRepositorio ?? new UsuarioRepositorio(_db); }
        }

        public ITarefaRepositorio TarefaRepositorio
        {
            get { return _tarefaRepositorio ?? new TarefaRepositorio(_db); }
        }

        public ISubTarefaRepositorio SubTarefaRepositorio 
        {
            get { return _subTarefaRepositorio ?? new SubTarefaRepositorio(_db); }
        }
        
        public void Salvar() {
            try {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex) {
                var entry = ex.Entries.Single();
                entry.OriginalValues.SetValues(entry.GetDatabaseValues());
            }
            
            catch (Exception ex) {
                _db.Rollback();
                throw;
            }
        }

        public async Task SalvarAsync() {
            try {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex) {
                var entry = ex.Entries.Single();
                entry.OriginalValues.SetValues(entry.GetDatabaseValues());
            }
            
            catch (Exception ex) {
                _db.Rollback();
                throw;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

       
    }
}
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoBRQ.Data.Repositorio.PaginacaoEntity
{
    public static class PagedListExtensions
    {
        public static PagedList<T> ToPagedList<T>(this List<T> source, int pagina,int tamanhoPagina)
        {
            return new PagedList<T>(source, pagina, tamanhoPagina);
        }
    }
}
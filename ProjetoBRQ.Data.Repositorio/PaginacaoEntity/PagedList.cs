using System.Collections.Generic;
using System.Linq;

namespace ProjetoBRQ.Data.Repositorio.PaginacaoEntity
{
    public class  PagedList<T> : List<T>, IPagedList
    {
        public int QuantidadeTotal { get; private set; }
        public int QuantidadePagina { get; private set; }
        public int Pagina { get; private set; }
        public int TamanhoPagina { get; private set; }

        public PagedList(List<T> source,int pagina, int tamanhoPagina)
        {
            QuantidadeTotal = source.Count();
            QuantidadePagina = GetPageCount(tamanhoPagina, QuantidadeTotal);
            Pagina = pagina  < 1 ? 0 : pagina ;
            TamanhoPagina = tamanhoPagina;
            AddRange(source.Skip(Pagina * TamanhoPagina).Take(TamanhoPagina).ToList());
        }

        private int GetPageCount(int tamanhoPagina, int quantidadeTotal)
        {
            if (tamanhoPagina == 0)
                return 0;

            var restante = quantidadeTotal % tamanhoPagina;
            return (quantidadeTotal / tamanhoPagina) + (restante == 0 ? 0 : 1);

        }
    }
}
namespace ProjetoBRQ.Data.Repositorio.PaginacaoEntity
{
    public interface IPagedList
    {
        int QuantidadeTotal { get; }
        int QuantidadePagina { get; }
        int Pagina { get; }
        int TamanhoPagina { get; }
    }
}